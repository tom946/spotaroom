# SPOTAROOM

## Decision/supositions:

**Time spend: 3h**

The menu (`nav`) in the mobile view should stay. Also I changed the main font-family since using the same as style was awful for titles.

I didn't push too much details on accesbility since it's just a test and I didn't want to expend more time than 3h (I could improve if it was necessary). Also I am quite fan of css-utilities (tailwind.css) and I would love to refactor it with it, I could use also hooks but since I am learing it I didn't want to push stuff that I am not 100% that how it would work. 

Stuff I would like to add but I didn't have time to: 
- Accessibility
- CSS Utilities
- Pagination
- Using hooks (Home.tsx)
- Tests (_Since most of the components, 3, are just visual component I thought it wasn't necessary or primordial_ ¯\\_(ツ)_/¯)


---

## How to...


1. How to run the project?

```bash
# Install dependencies
$ yarn # or `npm i`

# Run server & web-client
$ yarn start
```

2. How to test the project?
```bash
$ yarn test
```

3. How to deploy
```bash
# Build the project
$ yarn build
```

> Upload the dist folder wherever you want. 
> Eg. firebase, heroku, github-pages

> *Make sure to upload server index.js file and update cors policy
