import { Homecard } from "../../domain";

export type Props = {
    homecard: Homecard
}