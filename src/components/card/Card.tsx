import React from "react";
import { Props } from "./Cards.types";
import "./Card.css";

export function Card({ homecard }: Props) {
  return (
    <li className="c-card">
      <img
        className="c-card__img"
        src={homecard.photoUrls.homecardHidpi}
        title={homecard.title}
      />
      <article className="c-card__container">
        <section className="c-card__description">
          <h3 className="c-card__title">{homecard.title}</h3>
          <p className="c-card__price">
            {homecard.pricePerMonth.toLocaleString("es-ES", {
              style: "currency",
              currency: "EUR",
              minimumFractionDigits: 0
            })}
          </p>
        </section>
        <section className="c-card__actions">
          <button type="button" className="c-card__button">Book now!</button>
          <button type="button" className="c-card__button c-card__button--more-details">
            More details
          </button>
        </section>
      </article>
    </li>
  );
}
