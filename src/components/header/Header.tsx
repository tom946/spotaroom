import React from "react";
import './Header.css';

export function Header() {
  return (
    <header className="c-header">
      <h1 className="c-header__title">Spotaroom</h1>
      <nav className="c-header__nav">
        <a href="#">The company</a>
        <a href="#">How we work</a>
        <a href="#">Contact us</a>
      </nav>
    </header>
  );
}
