export interface Homecard {
  description: null;
  thingsWeLike: null;
  thingsToKeepInMind: null;
  id: number;
  features: Features;
  adId: number;
  roomId: number;
  type: string;
  title: string;
  mainPhotoUrl: string;
  photoUrls: PhotoUrls;
  pricePerMonth: number;
  monthlyPrice: Features;
  visited: boolean;
  neighborhoodName: string;
  runnerName: null;
  url: string;
  favorite: boolean;
  checked: boolean;
  currencySymbol: string;
  currencyLabel: string;
  currencyIsoCode: string;
  sharedRoom: boolean;
  roomType: number[];
  aptFeatures: number[];
  roomFeatures: number[];
  suitFeatures: number[];
  relevance: null;
  city: string;
  refreshAvailabilitiesDate: string;
  reviews: Reviews;
  instantBooking: Features;
  isNew: boolean;
  firstAvailableDate: string;
}

interface PhotoUrls {
  homecardHidpi: string;
  homecard: string;
}

interface Features {}

interface Reviews {
  count: number;
  ratingAverage: number;
}
