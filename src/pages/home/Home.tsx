import React, { Component } from "react";
import { State, Props } from "./Home.types";
import { Card } from "../../components/card";
import { Header } from "../../components/header";
import "./Home.css";

class Home extends Component<Props, State> {
  state: State = {
    homecards: []
  };
  async componentDidMount() {
    const response = await fetch("/homecards");
    const { homecards } = await response.json();
    this.setState({ homecards });
  }
  render() {
    const { homecards } = this.state;
    return (
      <React.Fragment>
        <Header />
        <main className="c-home">
          <ol className="c-home__list">
            {!homecards.length && <div>Loading...</div>}
            {homecards.map(homecard => (
              <Card key={homecard.id} homecard={homecard} />
            ))}
          </ol>
        </main>
      </React.Fragment>
    );
  }
}

export default Home;
