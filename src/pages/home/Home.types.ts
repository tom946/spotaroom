import { Homecard } from "../../domain";

export type State = {
  homecards: Homecard[];
};

export type Props = {};
