const express = require("express");
const fetch = require("node-fetch");
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

async function fetchIds() {
  const url =
    "https://frontend-interview.spotahome.com/api/public/listings/search/markers/madrid";
  const response = await fetch(url);
  const { data } = await response.json();
  return data.slice(0, 30).map(property => property.id); // [134, 12, 1223]
}

async function fetchHouses(ids) {
  const payload = ids.join("&ids[]=");
  const url =
    "https://frontend-interview.spotahome.com/api/public/listings/search/homecards_ids?ids[]=" +
    payload;
  const response = await fetch(url);
  const { data: homecards } = await response.json();
  return homecards;
}

app.get("/homecards", async (req, res) => {
  const ids = await fetchIds();
  const homecards = await fetchHouses(ids);
  res.json(homecards);
});

const hostname = "127.0.0.1";
const port = 1234;
app.listen(port, () => {
  console.log(`Serving running at http://${hostname}:${port}/`);
});
